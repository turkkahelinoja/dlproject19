# Get downloader
`git clone git@github.com:harshilpatel312/open-images-downloader.git`

# Get the training data
```
wget https://requestor-proxy.figure-eight.com/figure_eight_datasets/open-images/train-images-boxable.csv
wget https://requestor-proxy.figure-eight.com/figure_eight_datasets/open-images/train-annotations-bbox.csv
```


# Get the test data
```
wget https://requestor-proxy.figure-eight.com/figure_eight_datasets/open-images/test-annotations-bbox.csv
wget https://requestor-proxy.figure-eight.com/figure_eight_datasets/open-images/test-images.csv
```

# Get labelmap
`wget https://requestor-proxy.figure-eight.com/figure_eight_datasets/open-images/class-descriptions-boxable.csv`

# Install requirements for downloader
`pip install tqdm`

# fetch images for our labels
```
python ../../open-images-downloader/downloader/download.py --images test-images.csv --annots test-annotations-bbox.csv --objects 'Coffee cup' 'Coffee' 'Coffee table' --dir ./images/test --labelmap class-descriptions-boxable.csv
python ../../open-images-downloader/downloader/download.py --images train-images-boxable.csv --annots train-annotations-bbox.csv --objects 'Coffee cup' 'Coffee' 'Coffee table' --dir ./images/train --labelmap class-descriptions-boxable.csv
```

