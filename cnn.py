import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as npp
import matplotlib.pyplot as plt
import pandas as pd

# Set default tensor type to cuda if cuda support is available
if torch.cuda.is_available():
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
else:
    torch.set_default_tensor_type('torch.FloatTensor')

class CNN(nn.Module):
    """Convolutional Neural Network"""

    def __init__(self, in_channels=3, out_channels=4, kernel_size=7):
        super(CNN, self).__init__()
        self.conv_net1 = nn.Sequential(
            # 1st block
            nn.Conv2d(in_channels, 64, kernel_size, stride=2),
            nn.LeakyReLU(),
            nn.Conv2d(64, 32, 3),
            nn.LeakyReLU(),
            nn.Conv2d(32, 64, 1),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        self.conv_net2 = nn.Sequential(
            # 2nd block
            nn.Conv2d(64, 128, 3),
            nn.LeakyReLU(),
            nn.Conv2d(128, 64, 1),
            nn.LeakyReLU(),
            nn.Conv2d(64, 128, 3),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        self.conv_net3 = nn.Sequential(
            # 3rd block
            nn.Conv2d(128, 256, 1),
            nn.LeakyReLU(),
            nn.Conv2d(256, 128, 3),
            nn.LeakyReLU(),
            nn.Conv2d(128, 256, 1),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        self.conv_net4 = nn.Sequential(
            # 4th block
            nn.Conv2d(256, 512, 3),
            nn.LeakyReLU(),
            nn.Conv2d(512, 256, 1),
            nn.LeakyReLU(),
            nn.Conv2d(256, 512, 3),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        self.conv_net5 = nn.Sequential(
            # 5th block
            nn.Conv2d(512, 256, 1),
            nn.LeakyReLU(),
            nn.Conv2d(256, 512, 3),
            nn.LeakyReLU(),
            nn.Conv2d(512, 512, 1),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        self.conv_net6 = nn.Sequential(
            # 6th block
            nn.Conv2d(512, 512, 2),
            nn.LeakyReLU(),
            nn.Conv2d(512, 512, 2),
            nn.LeakyReLU(),
            nn.Conv2d(512, 512, 2),
            nn.LeakyReLU()
        )

        self.linear_net = nn.Sequential(
            nn.Linear(512*2*2, 4096),
            nn.LeakyReLU(),
            nn.Linear(4096, 7) # Output channels 7 = 4 corners of a bounding box as an output + 3 classes
        )

        #self.init_weights()


    def init_weights(self):
        """ Initializes the weights of the network """
        for m in self.modules():
            if type(m) is torch.nn.Linear:
                torch.nn.init.uniform_(m.weight)
                torch.nn.init.zeros_(m.bias)


    def forward(self, x):
        """ Runs data through the network
        
        Arguments:
            x {torch.tensor} -- Image in format [C, N, H, W]
                                [Channels, Batch Size, Height, Width]
        """

        batch_size = x.shape[0]
        
        x = self.conv_net1(x)
        x = self.conv_net2(x)
        x = self.conv_net3(x)
        x = self.conv_net4(x)
        x = self.conv_net5(x)
        x = self.conv_net6(x)

        x = x.view(batch_size, -1)
        x = self.linear_net(x)

        bbox = x[:,:4]
        class_prob = x[:, 4:]

        # return predicted bounding box coordinates and probabilities for each class
        return bbox, class_prob
