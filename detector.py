import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as npp
import matplotlib.pyplot as plt
import pandas as pd

from cnn import CNN

# Set default tensor type to cuda if cuda support is available
if torch.cuda.is_available():
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
else:
    torch.set_default_tensor_type('torch.FloatTensor')


class Detector():
    """ Detector for detecting objects from images. """

    def __init__(self):
        self.default_filename = "cnn"
        
        # Setup environment
        if torch.cuda.is_available():
            self.device = "cuda"
        else:
            self.device = "cpu"

        # Setup network
        net = CNN()
        self.net = net.to(self.device)
        self.optimizer = torch.optim.Adam(self.net.parameters(), lr=0.001)
        
        self.bbox_loss = nn.MSELoss()
        self.class_loss = nn.CrossEntropyLoss()


    def save_model(self, filename=None):
        """ Saves the weights of the network """
        if filename is None:
            filename = self.default_filename

        model_file = ''.join((filename, "_model.mdl"))
        torch.save(self.net.state_dict(), model_file)

        print("Saved model {}".format(model_file))

    
    def load_model(self, filename):
        """ Loads the weights of the network """
        state_dict = torch.load(filename)
        self.net.load_state_dict(state_dict)
        self.net = self.net.to(self.device)
        self.optimizer = torch.optim.Adam(self.net.parameters(), lr=0.001)


    def update_net(self):
        """ Update network """
        self.optimizer.step()
        self.optimizer.zero_grad()
        
