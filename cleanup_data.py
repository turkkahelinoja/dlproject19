import pandas as pd
import numpy as np
from os import listdir
from os.path import isfile, join

train_images = "./data/images/train"
test_images = "./data/images/test"

labels = ['Coffee table', 'Coffee', 'Coffee cup']
values = np.arange(0, 3)

# load files
train_ann = pd.read_csv("data/train-annotations-bbox.csv")
test_ann = pd.read_csv("data/test-annotations-bbox.csv")
labelmap = pd.read_csv("data/class-descriptions-boxable.csv", names=["labelid", "labelname"])

# create labelnames
labelnames = labelmap[labelmap.labelname.isin(labels)]
labellist = ['/m/02p5f1q', '/m/02vqfm', '/m/078n6m']

# find image ids of images we are going to use
train_ids = [f.rstrip(".jpg") for f in listdir(train_images) if isfile(join(train_images, f))]
test_ids = [f.rstrip(".jpg") for f in listdir(test_images) if isfile(join(test_images, f))]

# find matching ids from original annotations
train_ann_filtered = train_ann[train_ann.ImageID.isin(train_ids)]
test_ann_filtered = test_ann[test_ann.ImageID.isin(test_ids)]

# find only labels we use
train_ann_filtered = train_ann_filtered[train_ann_filtered.LabelName.isin(labelnames['labelid'])]
test_ann_filtered = test_ann_filtered[test_ann_filtered.LabelName.isin(labelnames['labelid'])]

train_ann_filtered = train_ann_filtered.replace(to_replace=labellist, value=values)
test_ann_filtered = test_ann_filtered.replace(to_replace=labellist, value=values)
# save filtered results to csv
train_ann_filtered.to_csv("data/train-annotations-bbox-filt.csv")
test_ann_filtered.to_csv("data/test-annotations-bbox-filt.csv")
