# Deep Learning project 2019

This system is a coffee cup detector to detect dirty coffee cups from video feed.

Installation:
- `git clone git@version.aalto.fi:helint3/dlproject19.git`
- `sudo apt update && sudo apt install virtualenv`
- `cd .. && virtualenv -p python3 venv.dlp`
- `. ./venv.dlp/bin/activate`
- `cd dlproject19 && pip install -r requirements.txt`
 
Running the system:
- `python training.py -ev -m path/to/your/model.mdl`

Training the system:
- `python training.py --verbose -bs <number-of-batches> -ep <number-of-epochs> -w <number-of-workers-in-dataloader>`

Files:
- cnn.py contains the actual neural network
- detector.py is an interface to use the neural network
- training.py is used for training and evaluating the network
- dataset.py is a data loader implementation for this problem
- image_download.md contains instructions for downloading the data
- cleanup_data.py is used for filter the downloaded data for this system
- requirements.txt contains required pip packages for running the software