from torch.utils.data import Dataset
import torch.nn as nn
import torch
from torchvision import transforms
import os
from skimage import io
from PIL import Image
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


"""
Container class for images with bounding box data. See https://pytorch.org/tutorials/beginner/data_loading_tutorial.html for reference
"""
class ObjectBBOXDataset(Dataset):
    """Object Bounding box dataset."""

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.annotations_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.annotations_frame)

    def __getitem__(self, idx):
        """
        Read single item from annotation file and return them as tensors
        """
        img_name = os.path.join(self.root_dir,
                                self.annotations_frame.iloc[idx, 1]) + ".jpg"
        image = Image.open(img_name)
        label = self.annotations_frame.iloc[idx, 3]
        bbox = self.annotations_frame.iloc[idx, 5:9].values
        bbox = bbox.astype('float').reshape(1, 4)
        image_arr = np.asarray(image)

        if len(image_arr.shape) != 3 or image_arr.shape[2] != 3:
            return self.__getitem__(idx+1)
        
        if self.transform:
            image = self.transform(image)

            image = np.asarray(image)
            image = torch.tensor(image, device='cpu', dtype=torch.float)
            image = image / 255
            image = image.view(3, 512, 512)

        sample = {'image': image, 'label':label, 'bbox': bbox}

        return sample
        

class ClassNamesDataset():
    """Class name dataset"""
    def __init__(self, csv_file):
        """
        Args:
            csv_file (string): Path to the csv file with class names.
        """
        self.class_names = pd.read_csv(csv_file, names=["label", "class_name"])
    
    def __len__(self):
        return len(self.class_names)

    def get_class_name(self, label):
        row = self.class_names[self.class_names["label"] == label]
        return row["class_name"]
