import os
import sys
import argparse

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
import numpy as npp
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pandas as pd

from cnn import CNN
from detector import Detector
from dataset import ObjectBBOXDataset, ClassNamesDataset

if torch.cuda.is_available():
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
else:
    torch.set_default_tensor_type('torch.FloatTensor')


def label_to_vector(labels):
    """ Converts labels to vectors of format [p1, p2, p3] """
    labels = torch.tensor(labels).view(-1, 1)
    print(labels)
    vectors = torch.zeros([labels.shape[0], 3])
    vectors[labels[:,0] == 0] = torch.tensor([1, 0, 0], dtype=torch.float)
    vectors[labels[:,0] == 1] = torch.tensor([0, 1, 0], dtype=torch.float)
    vectors[labels[:,0] == 2] = torch.tensor([0, 0, 1], dtype=torch.float)

    return vectors


def show_bounding_box_image(image, bbox):
    """Helper function for showing image with bounding box"""
    
    try:
        fig, ax = plt.subplots(1)
        plt.imshow(image)

        image_size = image.shape
        x_min, x_max, y_min, y_max = bbox
        width = x_max - x_min 
        height = y_max - y_min

        # Draw bounding box over the image
        box = patches.Rectangle((x_min*image_size[0], y_min*image_size[1]), width*image_size[0], height*image_size[1], fill=None, alpha=1)

        ax.add_patch(box)
        plt.show()

    except KeyboardInterrupt:
        plt.close()
        raise KeyboardInterrupt


def train(detector, train_loader, test_loader, epochs=5, verbose=False):
    """ Training loop """
    # Zero the parameters gradients
    detector.optimizer.zero_grad()

    try:

        # One epoch is one pass through the training data
        for epoch in range(epochs):
            # Training statistics
            running_loss = 0.0
            running_class_loss = 0.0
            running_bbox_loss = 0.0
            print_every = 32
            # Set the network to training mode
            detector.net.train()

            # Load batches using DataLoader
            for i, values in enumerate(train_loader, 0):
                images, bboxes = values["image"].to(detector.device), values["bbox"].to(detector.device)
                images, bboxes = images.float(), bboxes.float()
                labels = values["label"].to(detector.device)
                labels = labels.long()

                output_bboxes, output_prob = detector.net(images)
                
                # Calculate bbox and class losses and sum them together
                bbox_loss = detector.bbox_loss(output_bboxes, bboxes.view(-1, 4))
                class_loss = detector.class_loss(output_prob, labels)
                loss = bbox_loss + class_loss
                loss.backward()

                # Update network parameters only every 32 batches
                if (i % 32) - 1 == 0:            
                    detector.update_net()

                # Print statistics
                if verbose:
                    running_bbox_loss += bbox_loss.item()
                    running_class_loss += class_loss.item()
                    running_loss += loss.item()
                    if (i % print_every) == (print_every - 1):
                        print("[{}, {}] loss: {:.2f}, bbox loss: {:.2f}, class loss: {:.2f}".format(
                            epoch+1, i+1, running_loss/print_every, running_bbox_loss/print_every, class_loss/print_every))
                        running_loss = 0.0
                        running_bbox_loss = 0.0
                        running_class_loss = 0.0
                        
            detector.update_net()

    # Save model even if training is interrupted
    except KeyboardInterrupt:
        filename = "cnn_interrupted"
        print("Keyboard interrupt. Saving model to", filename)
        detector.save_model(filename=filename)
        sys.exit()

    print("Finished training")

        
def test(detector, test_loader, verbose=False):
    """ Test the network """
    detector.net.eval()

    try:
        with torch.no_grad():
            for values in test_loader:
                images, bboxes = values["image"].to(detector.device), values["bbox"].to(detector.device)
                images, bboxes = images.float(), bboxes.float()
                labels = values["label"].to(detector.device)
                labels = labels.long()

                output_bboxes, output_prob = detector.net(images)
                predicted_label = torch.argmax(F.softmax(output_prob, dim=-1))
                print("True label: {}, predicted label: {}".format(labels.item(), predicted_label))

                images = images.view(512, 512, 3)
                output_bboxes = output_bboxes.view(-1, 1)
                show_bounding_box_image(images.cpu(), output_bboxes.cpu())

    except KeyboardInterrupt:
        print("Keyboard interrupt. Exiting.")
        sys.exit()


def parse_arguments():
    """ Parses command line arguments """
    parser = argparse.ArgumentParser()
    parser.add_argument("-dd", "--data-dir", default="./data/", type=str, help="Path to data directory.")
    parser.add_argument("-v", "--verbose", action="store_true", default=False, help="Increase the output verbosity")
    parser.add_argument("-ev", "--evaluate", action="store_true", default=False, help="Evaluate model. If false, train instead.")
    parser.add_argument("-m", "--model", type=str, default=None, help="Path to model.")
    parser.add_argument("-tra", "--train-annotations", type=str, default="train-annotations-bbox-filt.csv", help="Path to training annotations.")
    parser.add_argument("-tea", "--test-annotations", type=str, default="test-annotations-bbox-filt.csv", help="Path to test annotations.")
    parser.add_argument("-cla", "--class-names", type=str, default="class-descriptions-boxable.csv", help="Path to class names.")
    parser.add_argument("-bs", "--batch-size", type=int, default=32, help="Batch size for training.")
    parser.add_argument("-ep", "--epochs", type=int, default=5, help="Epochs for training the data. One epoch is one loop through the training data.")
    parser.add_argument("-w", "--num-workers", type=int, default=4, help="Specify number of workers for dataloading")
    
    return parser.parse_args()


def main():
    args = parse_arguments()

    verbose = args.verbose
    evaluate = args.evaluate
    filename = args.model
    data_dir = args.data_dir
    if not os.path.isdir(data_dir):
        print("Directory not found. Exiting")
        return 1
    
    # Empty CUDA cache
    torch.cuda.empty_cache()

    # Load model
    detector = Detector()
    if filename is not None:
        detector.load_model(filename)
    
    # Load data
    train_annotations_filename = args.train_annotations
    test_annotations_filename = args.test_annotations
    class_names_filename = args.class_names

    # Apply image preprocessing transformations. Normalize color values to 0-1 range and scale images to uniform size.
    transform = transforms.Compose([
        transforms.Resize((512, 512))
        ])
    
    # Load datasets
    train_dataset = ObjectBBOXDataset(csv_file=data_dir + train_annotations_filename, root_dir=data_dir + "images/train", transform=transform)
    test_dataset = ObjectBBOXDataset(csv_file=data_dir + test_annotations_filename, root_dir=data_dir + "images/test", transform=transform)
    class_names_dataset = ClassNamesDataset(csv_file=data_dir + class_names_filename)

    # Load datasets into dataloaders
    train_loader = DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True, num_workers=args.num_workers)  # Batch sizes are complitely arbitrary at this point
    test_loader = DataLoader(test_dataset, batch_size=1, shuffle=True, num_workers=args.num_workers)

    # Train or evaluate
    if evaluate:
        test(detector, test_loader)
    else:
        train(detector, train_loader, test_loader, epochs=args.epochs, verbose=args.verbose)
        
        detector.save_model()



if __name__ == "__main__":
    main()
